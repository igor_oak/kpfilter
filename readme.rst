KEGG Pathway Xml Filter
=======================

Algoritmo
---------

1. Obter todas as reações filtrando *entries* utilizando genes de entrada (enzimas identificadas no organismo) e/ou fornecendo reações extras.
2. Com a lista de reações, filtra-se os compostos.
3. Filtra-se o xml da via usando as reações e compostos (que foram obtidos em função dos indentificadores de entrada).
4. Gera-se o novo xml customizado.
5. O novo xml customizado é convertido para `SBML`_ usando o conversor `KEGGTranslator`_.

.. _KEGGTranslator: http://www.ra.cs.uni-tuebingen.de/software/KEGGtranslator/doc/index.html
.. _SBML: http://sbml.org/Main_Page