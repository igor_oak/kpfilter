#!/usr/bin/env python3.4
from argparse import ArgumentParser
from csv import reader
from lxml import etree
from urllib.request import urlopen
from xml.dom.minidom import parseString

import sys

def _get_stream(uri):
    """
    Try guess uri and returns correct stream.
    """

    try:
        return open(uri)

    except FileNotFoundError:
        url = 'http://rest.kegg.jp/get/{}/kgml'.format(uri)
        return urlopen(url)

class KEGGPathwayFilter():
    """
    An entry based, xml simple filter.
    """

    def __init__(self, uri):
        with _get_stream(uri) as stream:
            self.etree = etree.parse(stream)

    def find_entries(self, entry_names=[]):
        """
        Find entry nodes set by name. Entry node example:

        <entry id="19" name="ean:Eab7_0377" type="gene" reaction="rn:R04966"
            link="http://www.kegg.jp/dbget-bin/www_bget?ean:Eab7_0377">
            <graphics name="Eab7_0377" fgcolor="#000000" bgcolor="#BFFFBF"
                 type="rectangle" x="895" y="606" width="46" height="17"/>
        </entry>
        """

        entry_nodes = self.etree.findall('entry')
        entry_ids = set()
        for entry_name in entry_names:
            for entry_node in entry_nodes:
                if entry_name in entry_node.attrib['name']:
                    entry_ids.add(entry_node.attrib['id'])

        entries = []
        for entry_id in entry_ids:
            entries.extend(entry for entry in entry_nodes if entry_id == entry.attrib['id'])

        return entries

    def find_reactions(self, entries=[], reaction_names=[]):
        """
        Find reaction nodes set using id of entries and reactions. Reaction node example:

        <reaction id="19" name="rn:R04966" type="reversible">
            <substrate id="182" name="cpd:C05760"/>
            <product id="183" name="cpd:C05761"/>
        </reaction>
        """

        reaction_ids = []
        for _id in map(lambda e: e.attrib['id'], entries):
            if not _id in reaction_ids:
                reaction_ids.append(_id)

        for reaction_name in reaction_names:
            reaction_ids.append(self.etree.find('./reaction/[@name="{}"]'.format(reaction_name)).attrib['id'])

        reactions = []
        for _id in reaction_ids:
            reactions.extend(self.etree.findall('./reaction/[@id="{}"]'.format(_id)))

        return reactions

    def find_compounds(self, reactions):
        """
        Find compound nodes set using reaction node childrens id. Compound node example:

        <entry id="182" name="cpd:C05760" type="compound"
            link="http://www.kegg.jp/dbget-bin/www_bget?C05760">
            <graphics name="C05760" fgcolor="#000000" bgcolor="#FFFFFF"
                 type="circle" x="870" y="559" width="8" height="8"/>
        </entry>
        """

        compounds = []
        compound_ids = []
        for reaction_entry in reactions:
            for child_entry in reaction_entry:
                # avoid duplicates
                _id = child_entry.attrib['id']
                if not _id in compound_ids:
                    compound_ids.append(_id)

        for _id in compound_ids:
            compounds.extend(self.etree.findall('./entry/[@id="{}"]'.format(_id)))

        return compounds

    def find_relations(self, entries):
        """
        Find all relations nodes using entry id. Relation node example:

        <relation entry1="262" entry2="53" type="ECrel">
            <subtype name="compound" value="157"/>
        </relation>
        """

        relations = []
        for entry in entries:
            relations.extend(self.etree.findall('./relation/[@entry1="{}"]'.format(entry.attrib['id'])))

        return relations

    def find_nodes(self, entry_names=[], reaction_names=[]):
        """
        Filter nodes set based on entry_names and reaction_names.
        """

        entries = self.find_entries(entry_names)
        reactions = self.find_reactions(entries, reaction_names)
        compounds = self.find_compounds(reactions)
        relations = self.find_relations(entries)

        doc = etree.Element('pathway', self.etree.getroot().attrib)
        doc.extend(entries)
        doc.extend(reactions)
        doc.extend(compounds)
        doc.extend(relations)

        return doc

    def write_xml(self, entry_names=[], reaction_names=[], file=None, doctype=None):
        """
        Dump entries in file.
        """

        file = open(file, 'wt') if file else sys.stdout
        if entry_names or reaction_names:
            document = self.find_nodes(entry_names, reaction_names)
            doctype = doctype or '<!DOCTYPE pathway SYSTEM "http://www.kegg.jp/kegg/xml/KGML_v0.7.1_.dtd">'
            xml = etree.tostring(document, encoding='unicode', doctype=doctype)
            xml = parseString(xml.replace('\n', '').replace('    ', '')).toprettyxml('    ', encoding='utf-8')

        else:
            xml = etree.tostring(self.etree)

        print(xml.decode('utf-8'), file=file)


if __name__ == '__main__':
    parser = ArgumentParser(description='Entry based KEGG xml filter.', prog='Kegg Pathway Filter')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-ed', '--entry-data', dest='entry_data', help='CSV file containing the entry names.')
    group.add_argument('-e', '--entries', help='Entry names.', nargs='+', default=[])

    group2 = parser.add_mutually_exclusive_group()
    group2.add_argument('-rd', '--reaction-data', dest='reaction_data', help='CSV file containing the reaction names.')
    group2.add_argument('-r', '--reactions', help='Reaction names.', nargs='+', default=[])

    parser.add_argument('-o', '--output', help='Output file. Default stdout.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')
    parser.add_argument('input', help='Input uri (file path or identifier).')
    args = parser.parse_args()

    kegg_filter = KEGGPathwayFilter(args.input)
    if args.entry_data:
        _entry_names = []
        with open(args.entry_data, newline='') as entry_data:
            for row in reader(entry_data):
                _entry_names.extend(row)

        args.entries = _entry_names

    if args.reaction_data:
        _reaction_names = []
        with open(args.reaction_data, newline='') as reaction_data:
            for row in reader(reaction_data):
                _reaction_names.extend(row)

        args.reactions = _reaction_names

    kegg_filter.write_xml(args.entries, args.reactions, args.output)